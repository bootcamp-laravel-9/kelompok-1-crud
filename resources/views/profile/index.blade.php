@extends('layout.main')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Profile</h3>
        </div>

        <form action="{{ url('/profile-update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="text-center mb-3">
                    @php $user = session('user'); @endphp
                    <img src="{{ asset($user['foto']) }}" class="profile-user-img img-fluid img-circle"
                        alt="{{ $user['name'] }}" style="border-radius: 50%; width: 200px; height: 200px;">
                </div>
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user['name'] }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $user['email'] }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username"
                        value="{{ $user['username'] }}" readonly>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $user['alamat'] }}"
                        readonly>
                </div>
                <div class="form-group d-none" id="fotoFormGroup">
                    <label for="foto">Foto</label>
                    <input type="file" class="form-control" id="foto" name="foto">
                </div>
            </div>

            <div class="card-footer">
                <button type="button" class="btn btn-secondary" id="editButton">Edit</button>
                <button type="button" class="btn btn-warning d-none" id="cancelButton">Batal Edit</button>
                <button type="submit" class="btn btn-primary d-none" id="updateButton">Update</button>
            </div>
        </form>

    </div>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.1.0/js/bootstrap.bundle.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        const editButton = document.getElementById('editButton');
        const cancelButton = document.getElementById('cancelButton');
        const updateButton = document.getElementById('updateButton');
        const inputs = document.querySelectorAll('.form-control');
        const fotoFormGroup = document.getElementById('fotoFormGroup');

        editButton.addEventListener('click', function() {
            inputs.forEach(input => input.removeAttribute('readonly'));
            editButton.classList.add('d-none');
            cancelButton.classList.remove('d-none');
            updateButton.classList.remove('d-none');
            fotoFormGroup.classList.remove('d-none');
        });

        cancelButton.addEventListener('click', function() {
            inputs.forEach(input => {
                input.setAttribute('readonly', true);
            });
            cancelButton.classList.add('d-none');
            editButton.classList.remove('d-none');
            updateButton.classList.add('d-none');
            fotoFormGroup.classList.add('d-none');
        });
    </script>
@endsection
