<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">

        <img src="{{ asset('AdminLTE/dist/img/AdminLTELogo.png') }}" class="brand-image img-circle elevation-3"
            style="opacity: 0.8" />
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @php
                    $user = session('user', false);
                @endphp
                <img src="{{ asset($user['foto']) }}" class="img-circle elevation-2" alt="User Image"
                    style="border-radius: 50%; object-fit: cover; width: 36px; height: 36px;">
            </div>
            <div class="info">
                @if (session('user'))
                    <a href="{{ url('/profile') }}" class="d-block">{{ session('user')['name'] }}</a>
                @else
                    <a href="{{ url('/login') }}" class="d-block">Login</a>
                @endif
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                    aria-label="Search" />
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">

                    <a href="{{ url('/') }}" class="nav-link @yield('menu-dashboard')">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/member') }}" class="nav-link @yield('menu-data')">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Table Menu
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/bio') }}" class="nav-link @yield('menu-bio')">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Bio
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
