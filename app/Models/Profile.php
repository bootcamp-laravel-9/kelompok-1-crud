<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profiles_users';
    protected $fillable = [
        'name',
        'email',
        'username',
        'alamat',
        'password',
        'foto'
    ];

    protected $hidden = [
        'password',
    ];
}
