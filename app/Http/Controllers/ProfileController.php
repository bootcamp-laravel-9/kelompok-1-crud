<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\ProfilesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = ProfilesUsers::find(session('user')['id']);

        return view('profile.index', compact('user'));
    }

    public function show($id)
    {
        $profile = Profile::findOrFail($id);
        return view('profile.show', compact('profile'));
    }

    public function update(Request $request)
    {
        $userId = session('user')['id'] ?? null;

        if (!$userId) {
            return redirect('/login')->with('error', 'You must be logged in to update the profile.');
        }

        $profile = ProfilesUsers::find(session('user')['id']);

        if (!$profile) {
            return redirect()->back()->with('error', 'Profile not found.');
        }

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'username' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
            'foto' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->username = $request->username;
        $profile->alamat = $request->alamat;

        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $uniqueName = Str::uuid() . '.' . $file->extension();
            $file->move(public_path('images'), $uniqueName);

            $profile->foto = 'images/' . $uniqueName;
        }

        if ($profile->save()) {
            $updatedUserData = $profile->toArray();
            session(['user' => $updatedUserData]);

            return redirect()->intended('profile')->with('success', 'Successful Update the Profile');
        } else {
            return redirect()->back()->with('error', 'Failed to update profile.')->withInput();
        }
    }
}
