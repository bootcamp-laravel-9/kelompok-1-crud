<?php

namespace App\Http\Controllers;

use App\Models\ProfilesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth/login');
    }

    public function register()
    {
        return view('auth/register');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'max:24'],
            'email' => ['required', 'max:100', 'email', 'unique:profiles_Users,email'],
            'username' => ['required', 'max:25', 'alpha_dash', 'unique:profiles_Users,username'],
            'alamat' => ['required', 'max:100'],
            'password' => ['required', 'min:8', 'max:15', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]+$/'],
            'password_confirmation' => ['required', 'same:password'],
            'foto' => []
        ]);

        $user = ProfilesUsers::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'alamat' => $request->alamat,
            'password' => Hash::make($request['password']),
            'foto' => $request->foto,
        ]);

        return redirect('/login')->with('success', 'Register Berhasil');
    }

    public function show(Request $request)
    {
        $request->validate([
            'email' => ['required', 'exists:profiles_users,email'],
            'password' => ['required'],
        ], [
            'email.exists' => 'Email/Username tidak ditemukan.',
        ]);

        $user = ProfilesUsers::where('email', $request->email)
            ->first();
        // dd($user);
        if (!$user) {
            return redirect('/login')->with('error', 'Email/Username Tidak Ditemukan');
        }

        $isValidPassword = Hash::check($request->password, $user->password);
        if (!$isValidPassword) {
            return redirect('/login')->with('error', 'Password salah');
        }

        $token = $user->createToken(config('app.name'))->plainTextToken;
        $request->session()->put('LoginSession', $token);
        $request->session()->put('user', $user);
        return redirect('/')->with('success', 'Login Berhasil');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
