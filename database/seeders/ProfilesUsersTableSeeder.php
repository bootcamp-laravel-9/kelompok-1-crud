<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class ProfilesUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert a single dummy user
        DB::table('profiles_users')->insert([
            'name' => 'Satya',
            'email' => 'danielsatya@gmail.com',
            'username' => 'satyadaniel',
            'alamat' => '1234 Main Street',
            'password' => Hash::make('12345678'),
            'foto' => 'https://media.licdn.com/dms/image/D5603AQGXyjlUQVfWCA/profile-displayphoto-shrink_800_800/0/1708012861716?e=1717027200&v=beta&t=XQ7H9ZcEcnuCG8T7g_yx8a1dFD3ug_kMhVKAYsmgop8',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
