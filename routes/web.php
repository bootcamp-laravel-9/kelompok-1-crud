<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['session.login']], function () {
    Route::get('/', function () {
        return view('dashboard/index');
    })->name('dashboard');

    Route::get('/profile', [ProfileController::class, 'index']);
    // Route::get('/profiles/{id}', [ProfileController::class, 'show']);
    Route::post('/profile-update', [ProfileController::class, 'update']);
    Route::get('/login', [AuthController::class, 'login']);
    Route::post('/loginUser', [AuthController::class, 'show']);
    Route::get('/register', [AuthController::class, 'register']);
    Route::post('/registerUser', [AuthController::class, 'create']);
    Route::get('/logout', [AuthController::class, 'logout']);
});
